# Application for manipulating PostgreSQL database using Hibernate ORM.

Based on documentation from Tutorialspoint:
https://www.tutorialspoint.com/hibernate/index.htm


- CD into "JavaHibernate" directory:  
`cd JavaHibernate`


- Compile source code into "bin" directory:  
`javac -d bin src/*.java`


- Run compiled code with jar dependencies:  
`java -cp "bin;lib/*" Main`
